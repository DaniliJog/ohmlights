// board import
#include <ESP8266WiFi.h>

// third party stuff
#include <PubSubClient.h>
#include <FastLED.h>

// temporary config stuff
#include "secrets.h"



/* WIFI stuff
 * * * * * */ 

void wifi_setup() {
	Serial.print("[INFO] Starting wifi connect to: ");
	Serial.println(WIFI_SSID);
	WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

	while (WiFi.status() != WL_CONNECTED) {
		// wait for connect
		delay(500);
		Serial.print(".");
	}

	Serial.print("\n[INFO] Got IP address: ");
	Serial.println(WiFi.localIP());
}

void wifi_update() {
  /* Redo setup if disconnected. Blocks until connected.
   * */
	if (WiFi.status() != WL_CONNECTED) {
		Serial.println("[ERROR] Wifi disconnect.");
		wifi_setup();
	}
}



/* MQTT stuff
 * * * * * */

WiFiClient espClient;
PubSubClient client(espClient);
#define MQTT_SERVER "192.168.0.94"
#define MQTT_PORT 1883
#define MQTT_TOPIC "topic"

void mqtt_callback(char* topic, byte* payload, unsigned int length) {
	/* Is called on MQTT message arrival.
	 * */
	Serial.print("[INFO] Message arrived [");
	Serial.print(topic);
	Serial.print("] ");
	for (int i = 0; i < length; i++) {
		Serial.print((char)payload[i]);
	}
	Serial.println();
}

bool mqtt_connect() {
	Serial.print("[INFO] Connecting to MQTT broker: ");
  Serial.println(MQTT_SERVER);
	client.setServer(MQTT_SERVER, MQTT_PORT);
	client.setCallback(mqtt_callback);

  // Create a random client ID
  String clientId = "Ω+💡 " + String(random(0xffff), HEX);

  if (!client.connect(clientId.c_str())) {
    Serial.print("[ERROR] MQTT connect failed, rc=");
    Serial.println(client.state());
    return false;
  }
  Serial.print("[DEBUG] Subscribing to ");
  Serial.println(MQTT_TOPIC);

  // subscribe to topic
  if (!client.subscribe(MQTT_TOPIC)) {
    Serial.println("[ERROR] Failed to subscribe.");
    return false;
  }

  // publish hello to topic
  if (!client.publish(MQTT_TOPIC, "Hi there!")) {
    Serial.println("[ERROR] Failed to publish hello.");
    return false;
  }

  // success
  return true;
}

void mqtt_setup() {
	/* Connects to MQTT server and hooks up callback
	 * */
  mqtt_connect();
}

bool mqtt_update() {
	/* Blocking MQTT reconnect.
   * Calls queud messages
   * */
  bool result = true;
	if (!client.connected()) {
		Serial.println("[ERROR] MQTT disconnected. Reconnecting...");
    result = mqtt_connect();
	}

  // act on mqtt stuff
	client.loop();
  return result;
}



/* LED stuff
 * * * * * */

#define NUM_LEDS 150
#define DATA_PIN 5
CRGB leds[NUM_LEDS];

void led_setup() {
  /* Set up FastLED stuff.
   * */
	Serial.println("[DEBUG] Starting LED setup.");
	FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
}

void led_update(bool mqtt_connected) {
  /* Set LED state and show using FastLED.
   * */
  if (mqtt_connected) {
    set_all(CHSV(224, 255, 255));
  } else {
    set_all(CRGB::White);
  }
	FastLED.show();
}

void set_all(CRGB led) {
  /* Side-effectful function that sets `leds` to be some CRGB object
   * */
	for(int i = 0; i < NUM_LEDS; i++) {
		leds[i] = led;
	}
}



/* Main stuff
 * * * * * */
void setup() {
	// sanity check delay allows reprogramming if accidently blowing power w/leds
	delay(2000);
	Serial.begin(9600);
	Serial.print("Hello, world!");

	// JUST DO IT
	randomSeed(micros());

	led_setup();
	wifi_setup();
	mqtt_setup();

  Serial.println("[DEBUG] Finished Setup.");
}


void loop() {
  bool mqtt_connected = mqtt_update();
	led_update(mqtt_connected);
	wifi_update();
}
