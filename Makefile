
FQBN=esp8266:esp8266:generic
PORT=/dev/ttyUSB0
SKETCH=./OhmLights
SERIAL_BAUD=9600

BUILDDIR=./build
BINFILE=$(BUILDDIR)/ohmlights.bin

compile:
	arduino-cli compile \
		--fqbn $(FQBN) \
		--output $(BINFILE) \
		$(SKETCH)

flash: compile
	arduino-cli upload \
		--port $(PORT) \
		--fqbn $(FQBN) \
		--input $(BINFILE) \
		$(SKETCH)

monitor:
	screen $(PORT) $(SERIAL_BAUD)

clean:
	rm -r $(BUILDDIR)/*
